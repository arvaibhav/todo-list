
var list = document.getElementById('todo-list-container').children;

//addEventListener for adding todo item
toadd.addEventListener('keydown', additem);
document.getElementById('todoadd-button').addEventListener('click', additem);

function additem(e) {
  // check either enter key[13] or mouse click
  if (e.which == 13 || e.type == 'click') {
    if (toadd.value.trim() !== '') {
      let newlist = document.createElement('div');
      newlist.className = 'todo-list-item';
      newlist.setAttribute('draggable', 'true');
      let listchk = document.createElement('input');
      listchk.className = 'todo-checkbox';
      listchk.setAttribute('type', 'checkbox');

      let listinput = document.createElement('div');
      listinput.className = 'todo-detail';
      listinput.innerHTML = toadd.value.trim();
      toadd.value = '';

      let listremove = document.createElement('button');
      listremove.className = 'todo-remove';
      listremove.innerHTML = 'X';
      newlist.innerHTML += listchk.outerHTML + listinput.outerHTML + listremove.outerHTML;
      document.getElementById('todo-list-container').appendChild(newlist);
      saveToSession();
    }
  }
}

// addEventListener for editing content
var itemListContainer = document.getElementById('todo-list-container');

itemListContainer.addEventListener('dblclick', editText);
itemListContainer.addEventListener('keydown', editText);

function editText(e) {
  // enable editing todo content
  if (e.target.className === "todo-detail") {
    e.target.contentEditable = true;
    if (e.which == 13) {
      // on enter disable
      e.target.contentEditable = false;
    }
  }
  // remove child on cross click
  if (e.target.className === "todo-remove") {
    let par = e.target.parentElement;
    itemListContainer.removeChild(par);
    saveToSession();
  }

}

// Events for filtering the contents of list container
document.getElementById('filterby-contaniner').addEventListener('click', filterList);

function filterList(e) {
  switch (e.target.id) {
    // show all items
    case 'filterby-all': {
      for (let i = 0; i < list.length; i++) {
        list[i].setAttribute('style', 'display:flex;');
      }
    }
    break;
    // filter active items
  case 'filterby-active': {
    for (let i = 0; i < list.length; i++)
      if (list[i].querySelector('.todo-checkbox').checked) {
        list[i].setAttribute('style', 'display:none;');
      }
    else {
      list[i].setAttribute('style', 'display:felx;');
    }
  }
  break;
  // filter completed items
  case 'filterby-completed': {
    for (let i = 0; i < list.length; i++)
      if (list[i].querySelector('.todo-checkbox').checked) {
        list[i].setAttribute('style', 'display:flex;');
      }
    else {
      list[i].setAttribute('style', 'display:none;');
    }
  }
  }
}

// Load session on pageload
try {
  var templist = sessionStorage.getItem('list').split('} , {');
  for (let i = 0; i < templist.length; i++) {
    let newlist = document.createElement('div');
    newlist.className = 'todo-list-item';
    newlist.setAttribute('draggable', 'true');
    let listchk = document.createElement('input');
    listchk.className = 'todo-checkbox';
    listchk.setAttribute('type', 'checkbox');
    let listinput = document.createElement('div');
    listinput.className = 'todo-detail';
    let text = templist[i].trim();
    if (i == 0)
      text = text.substr(1, text.length);
    if (i == templist.length - 1)
      text = text.substr(0, text.length - 1);
    listinput.innerHTML = text;
    let listremove = document.createElement('button');
    listremove.className = 'todo-remove';
    listremove.innerHTML = 'X';
    newlist.innerHTML += listchk.outerHTML + listinput.outerHTML + listremove.outerHTML;
    document.getElementById('todo-list-container').appendChild(newlist);
  }
} catch (e) {}

function saveToSession() {
  let sessionText = '';
  for (let i = 0; i < list.length; i++) {
    let text = list[i].querySelector('.todo-detail').innerText;
    // '} , {'  used to split in array
    if (i == 0)
      sessionText += '{' + text + '}';
    else sessionText += ' , {' + text + '}';
  }
  sessionStorage.setItem('list', sessionText);
}

// drag drop Events
itemListContainer.addEventListener('dragover', dropTarget);
itemListContainer.addEventListener('dragenter', dropTarget);
itemListContainer.addEventListener('drop', dragAt);
itemListContainer.addEventListener('dragstart', dragFrom);
itemListContainer.addEventListener('dragend', dragEnd);
// Specifying Drop Targets
function dragEnd(e)
{
    dragElement.className='todo-list-item';
}
function dropTarget(e) {
  e.preventDefault();
}
var dragElement;
function dragFrom(e) {
dragElement=e.target;
dragElement.className+=" hover-element";
}

function dragAt(e) {

  if(e.target.className==='todo-list-item')
  {

  const x=dragElement.querySelector('.todo-detail').innerText;
  const status=dragElement.querySelector('.todo-checkbox').checked;
  dragElement.querySelector('.todo-checkbox').checked=  e.target .querySelector('.todo-checkbox').checked;
    e.target .querySelector('.todo-checkbox').checked=status;

  dragElement.querySelector('.todo-detail').innerText=e.target .querySelector('.todo-detail').innerText;
  e.target. querySelector('.todo-detail').innerText=x;

   }
  else
  {const x=dragElement.querySelector('.todo-detail').innerText;
  const status=dragElement.querySelector('.todo-checkbox').checked;
  dragElement.querySelector('.todo-checkbox').checked=  e.target.parentElement.querySelector('.todo-checkbox').checked;
    e.target.parentElement.querySelector('.todo-checkbox').checked=status;

  dragElement.querySelector('.todo-detail').innerText=e.target.parentElement.querySelector('.todo-detail').innerText;
  e.target.parentElement.querySelector('.todo-detail').innerText=x;

  }

  saveToSession();
}
