var todoList = new Array();
try {
  todoList = JSON.parse(sessionStorage.getItem('list'));
  domRefresh();
} catch (e) {
  todoList = new Array();
}

//adding content
document.getElementById('todoadd-text').addEventListener('keydown', additem);

function additem(e) {
  // check either enter key[13] or mouse click
  if (e.which == 13) {
    let value = document.getElementById('todoadd-text').value.trim();
    if (value !== '') {
      addToDatabase(value, false);
      document.getElementById('todoadd-text').value = '';
    }
  }
}

//edit content
document.getElementById('todo-list-container').addEventListener('click', editText);

function editText(e) {

  // remove child on cross click
  if (e.target.className === "todo-remove") {
    removeFromDatabase(e.target.parentNode.getAttribute('position'));
  }
  if (e.target.className === "todo-checkbox") {
    editInDatabase(e.target.parentNode.getAttribute('position'), e.target.parentNode.querySelector('.todo-detail').innerText, e.target.checked);
  }
}


// --------------Functions on Database  -----------------------------------------//
function addToDatabase(value, isChecked) {
  let item = {
    'value': value,
    'isChecked': isChecked
  };
  todoList.push(item);
  domRefresh();
}

// Remove element from database
function removeFromDatabase(index) {
  todoList.splice(index, 1);
  domRefresh();
}

// Edit content of element in databse
function editInDatabase(index, editWith, isChecked) {
  todoList[index]['value'] = editWith;
  todoList[index]['isChecked'] = isChecked;
  domRefresh();
}

// Swap list in database
function swapInDatabase(indexFrom, indexTo) {
  var temp = todoList[indexFrom];
  todoList[indexFrom] = todoList[indexTo];
  todoList[indexTo] = temp;
  domRefresh();
}

//
function domRefresh() {
  // clear child nodes
  let parentNode = document.getElementById('todo-list-container');
  while (parentNode.firstChild) {
    parentNode.removeChild(parentNode.firstChild);
  }
  // add child nodes
  for (let i = 0; i < todoList.length; i++) {
    let newlist = document.createElement('li');
    newlist.className = 'todo-list-item';
    newlist.setAttribute('draggable', 'true');

    let listchk = document.createElement('input');
    listchk.className = 'todo-checkbox';
    listchk.setAttribute('type', 'checkbox');
    listchk.setAttribute(todoList[i]['isChecked'] ? 'checked' : 'unchecked', '');


    let listinput = document.createElement('div');
    listinput.className = 'todo-detail';
    listinput.innerHTML = todoList[i]['value'];


    let listremove = document.createElement('button');
    listremove.className = 'todo-remove';
    listremove.innerHTML = 'X';
    newlist.innerHTML += listchk.outerHTML + listinput.outerHTML + listremove.outerHTML;
    newlist.setAttribute('position', i);
    document.getElementById('todo-list-container').appendChild(newlist);
  }
  // update the database in session
  sessionStorage.setItem('list', JSON.stringify(todoList));

}
